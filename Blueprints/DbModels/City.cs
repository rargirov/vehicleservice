﻿using Blueprints.Interfaces;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    public class City : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("City")]
        public string Name { get; set; }

        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }
    }
}
