﻿using Blueprints.Interfaces;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class Color : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Color")]
        public string Name { get; set; }
    }
}