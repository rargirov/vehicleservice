﻿using Blueprints.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class Country : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Country")]
        public string Name { get; set; }

        public virtual ICollection<City> Cities { get; set; }
    }
}
