﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    [Table("Customer")]
    public class Customer : Person
    {
        [StringLength(50)]
        [DisplayFormat(NullDisplayText = "-", ApplyFormatInEditMode = true)]
        public string Company { get; set; }

        public virtual ICollection<CustomerVehicle> CustomerVehicles { get; set; }
    }
}
