﻿using Blueprints.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    public class CustomerVehicle : IEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Registration number")]
        public string RegistrationNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string VIN { get; set; }

        [Required]
        [ForeignKey("Customer")]
        public int PersonId { get; set; }

        public virtual Customer Customer { get; set; }

        [Required]
        [ForeignKey("Vehicle")]

        public int VehicleId { get; set; }

        public virtual Vehicle Vehicle { get; set; }

        [Required]
        [ForeignKey("Color")]
        public int ColorId { get; set; }

        public virtual Color Color { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }
    }
}
