﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    [Table("Employee")]
    public class Employee : Person
    {
        public decimal Salary { get; set; }

        public virtual ICollection<Repair> Repairs { get; set; }
    }
}
