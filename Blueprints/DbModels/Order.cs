﻿using Blueprints.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    public class Order : IEntity
    {
        private DateTime? createdOn;
        private DateTime? paidOn;

        public Order()
        {
            this.IsPaid = false;
        }

        public int Id { get; set; }

        public bool IsPaid { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal AdditionalPrice { get; set; }

        [DisplayName("Total price")]
        public decimal TotalPrice { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}", NullDisplayText = "Not paid")]
        public DateTime? PaidOn
        {
            get
            {
                if (paidOn == null && this.IsPaid)
                {
                    paidOn = DateTime.Now;
                }
                return paidOn;
            }
            private set { paidOn = value; }
        }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime CreatedOn
        {
            get
            {
                if (createdOn == null)
                {
                    createdOn = DateTime.Now;
                }
                return createdOn.Value;
            }
            set { createdOn = value; }
        }


        [Required]
        public int RepairId { get; set; }

        public virtual Repair Repair { get; set; }
    }
}
