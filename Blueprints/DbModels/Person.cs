﻿using Blueprints.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    public abstract class Person : IBaseEntity
    {
        public int Id { get; set; }

        [Display(Name = "Full Name")]
        public string Name
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
            set { }
        }

        [Required]
        [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Street { get; set; }

        [Required]
        [ForeignKey("City")]
        public int CityId { get; set; }

        public virtual City City { get; set; }
    }
}