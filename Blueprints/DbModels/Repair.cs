﻿using Blueprints.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Blueprints.DbModels
{
    public class Repair : IEntity
    {
        private DateTime? _startedOn;
        private DateTime? _completedOn;

        public Repair()
        {
            this.WorkingHours = 0;
        }

        public int Id { get; set; }

        // TODO delete for production 
        public string Name { get; set; }

        public string Description { get; set; }

        public int Mileage { get; set; }

        public bool IsCompleted { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}", NullDisplayText = "Pending...")]
        public DateTime? CompletedOn
        {
            get
            {
                if (_completedOn == null && this.IsCompleted)
                {
                    _completedOn = DateTime.Now;
                }
                return _completedOn;
            }
            private set { _completedOn = value; }
        }

        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime StartedOn
        {
            get
            {
                if (_startedOn == null)
                {
                    _startedOn = DateTime.Now;
                }
                return _startedOn.Value;
            }
            set { _startedOn = value; }
        }

        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal WorkingHours { get; set; }

        [DisplayName("Replacement Parts")]
        public string ReplacementPartIds { get; set; }

        [DisplayName("ReplacementParts total price")]
        public decimal ReplacementPartsSum { get; set; }

        [DisplayName("Labor price")]
        public decimal LaborPrice { get; set; }


        [Required]
        [ForeignKey("RepairType")]
        public int RepairTypeId { get; set; }

        public virtual RepairType RepairType { get; set; }


        [Required]
        public int CustomerVehicleId { get; set; }

        public virtual CustomerVehicle CustomerVehicle { get; set; }


        [Required]
        [ForeignKey("Employee")]
        public int PersonId { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
