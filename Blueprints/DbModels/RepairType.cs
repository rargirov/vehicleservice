﻿using Blueprints.Interfaces;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class RepairType : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Repair type")]
        public string Name { get; set; }

        [Required]
        [DisplayName("Price per hour")]
        public decimal PricePerHour { get; set; }
    }
}
