﻿using Blueprints.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class ReplacementPart : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Replacement Part")]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Part Number")]
        public string PartNumber { get; set; }

        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        [Required]
        public int ReplacementPartCategoryId { get; set; }

        public virtual ReplacementPartCategory ReplacementPartCategory { get; set; }

        [Required]
        public int ReplacementPartManufacturerId { get; set; }

        public virtual ReplacementPartManufacturer ReplacementPartManufacturer { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}
