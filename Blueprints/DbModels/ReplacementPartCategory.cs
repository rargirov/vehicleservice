﻿using Blueprints.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class ReplacementPartCategory : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Category")]
        public string Name { get; set; }

        public virtual ICollection<ReplacementPart> ReplacementParts { get; set; }
    }
}