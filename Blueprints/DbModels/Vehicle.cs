﻿using Blueprints.Interfaces;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blueprints.DbModels
{
    public class Vehicle : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string VehicleModel { get; set; }

        [DisplayName("Vehicle")]
        public string Name
        {
            get
            {
                string name = String.Empty;

                if (this.VehicleManufacturer != null && this.VehicleModel != null)
                {
                    var vehicleManufacturerName = this.VehicleManufacturer.Name;
                    if (vehicleManufacturerName.Length > 0 && this.VehicleModel.Length > 0)
                    {
                        name = String.Format("{0} {1} {2} {3}", vehicleManufacturerName, VehicleModel, Modifination, Year);
                    }
                }

                return name;
            }
            set { }
        }

        [Required]
        [StringLength(50)]
        public string Modifination { get; set; }

        [StringLength(50)]
        public string Body { get; set; }

        [StringLength(50)]
        public string Fuel { get; set; }

        [StringLength(50)]
        public string EngineName { get; set; }

        [StringLength(50)]
        public string EngineType { get; set; }

        [StringLength(50)]
        public string Transmission { get; set; }

        [Required]
        public int Year { get; set; }

        [Required]
        [ForeignKey("VehicleManufacturer")]
        public int VehicleManufacturerId { get; set; }

        public virtual VehicleManufacturer VehicleManufacturer { get; set; }

        [Required]
        [ForeignKey("VehicleType")]
        public int VehicleTypeId { get; set; }

        public virtual VehicleType VehicleType { get; set; }
    }
}