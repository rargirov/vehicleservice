﻿using Blueprints.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Blueprints.DbModels
{
    public class VehicleManufacturer : IBaseEntity
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [DisplayName("Manufacturer")]
        public string Name { get; set; }

        [Required]
        public int CountryId { get; set; }

        public virtual Country Country { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }
    }
}