﻿namespace Blueprints.Interfaces
{
    public interface IBaseEntity : IEntity
    {
        string Name { get; set; }
    }
}
