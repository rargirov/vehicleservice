﻿namespace Blueprints.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
