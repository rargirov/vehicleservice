﻿namespace Blueprints.Interfaces
{
    public interface IMenuItem
    {
        string Text { get; set; }

        string ControllerName { get; set; }

        string ActionName { get; set; }

        object RouteValues { get; set; }
    }
}