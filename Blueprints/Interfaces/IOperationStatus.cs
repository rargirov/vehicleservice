﻿namespace Blueprints.Interfaces
{
    public interface IOperationStatus
    {
        bool IsSuccessful { get; set; }
    }
}
