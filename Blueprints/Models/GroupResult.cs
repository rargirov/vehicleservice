﻿using System.Collections;
using System.Collections.Generic;

namespace Blueprints.Models
{
    public class GroupResult
    {
        public object Key { get; set; }

        public int Count { get; set; }

        public IEnumerable Items { get; set; }

        public IEnumerable<GroupResult> SubGroups { get; set; }
    }
}