﻿using Blueprints.Interfaces;

namespace Blueprints.Models
{
    public class MenuItem : IMenuItem
    {
        public string Text { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public object RouteValues { get; set; }
    }
}