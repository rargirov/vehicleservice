﻿using Blueprints.DbModels;
using System.Collections.Generic;
using System.Linq;
namespace Blueprints.ViewModels
{
    public class RepairViewModel
    {
        public RepairViewModel(Repair repair, Order order)
        {
            this.Repair = repair;
            this.Order = order;
        }
        public Repair Repair { get; set; }

        public Order Order { get; set; }

        public Dictionary<ReplacementPart, int> ReplacementParts { get; set; }

    }
}