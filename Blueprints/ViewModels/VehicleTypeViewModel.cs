﻿using Blueprints.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Blueprints.ViewModels
{
    public class VehicleTypeViewModel
    {
        public int VehicleTypeId { get; set; }

        public string VehicleTypeName { get; set; }

        public IEnumerable<IGrouping<string, Vehicle>> GroupedVehicles { get; set; }
    }
}