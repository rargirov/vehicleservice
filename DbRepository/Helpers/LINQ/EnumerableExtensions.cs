﻿using Blueprints.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DbRepository.Helpers.LINQ
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<GroupResult> GroupByMany<TElement>(this IEnumerable<TElement> elements,
            params Func<TElement, object>[] groupSelectors)
        {
            if (groupSelectors.Length > 0)
            {
                var selector = groupSelectors.First();
                var nextSelectors = groupSelectors.Skip(1).ToArray();
                return elements.GroupBy(selector)
                    .Select(
                        group => new GroupResult
                        {
                            Key = group.Key,
                            Count = group.Count(),
                            Items = group,
                            SubGroups = group.GroupByMany(nextSelectors)
                        }
                    );
            }
            else
            {
                return null;
            }
        }
    }
}
