﻿using Blueprints.DbModels;
using System.Collections.Generic;

namespace DbRepository.Interfaces
{
    public interface ICityRepository : IRepository<City>
    {
        IEnumerable<City> FindAllByCountry(int countryId);
    }
}
