﻿using Blueprints.DbModels;

namespace DbRepository.Interfaces
{
    public interface IColorRepository : IRepository<Color>
    {
    }
}
