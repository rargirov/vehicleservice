﻿using Blueprints.DbModels;
using System.Collections.Generic;

namespace DbRepository.Interfaces
{
    public interface ICountryRepository : IRepository<Country>
    {
        IEnumerable<Country> FindAllWithCities();
    }
}
