﻿using Blueprints.DbModels;

namespace DbRepository.Interfaces
{
    public interface ICustomerRepository : IRepository<Customer>
    {
    }
}
