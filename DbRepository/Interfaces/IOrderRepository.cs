﻿using Blueprints.DbModels;

namespace DbRepository.Interfaces
{
    public interface IOrderRepository: IRepository<Order>
    {
    }
}
