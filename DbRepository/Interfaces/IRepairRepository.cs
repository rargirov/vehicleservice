﻿using Blueprints.DbModels;
using Blueprints.Models;
using System.Collections.Generic;

namespace DbRepository.Interfaces
{
    public interface IRepairRepository : IRepository<Repair>
    {
        IEnumerable<Repair> FindAllByCustomer(int customerId);

        IEnumerable<GroupResult> GetGroupedEntities(IEnumerable<Repair> repairs);
    }
}
