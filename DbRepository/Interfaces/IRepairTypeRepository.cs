﻿using Blueprints.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Interfaces
{
    public interface IRepairTypeRepository : IRepository<RepairType>
    {
    }
}
