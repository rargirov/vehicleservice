﻿using Blueprints.DbModels;
using System.Collections.Generic;

namespace DbRepository.Interfaces
{
    public interface IReplacementPartRepository : IRepository<ReplacementPart>
    {
        Dictionary<ReplacementPart, int> GetReplacementParts(string replacementPartIds);
    }
}
