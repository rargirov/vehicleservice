﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DbRepository.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);

        void Remove(TEntity entity);

        void Update(TEntity entity, params string[] propsToUpdate);

        TEntity FindById(int id);

        TEntity FindSingle(Expression<Func<TEntity, bool>> predicate);

        IEnumerable<TEntity> FindAll();

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
    }
}
