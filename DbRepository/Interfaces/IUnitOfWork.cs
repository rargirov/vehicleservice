﻿using Blueprints.Interfaces;
using Blueprints.Models;
using System;

namespace DbRepository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IColorRepository Color { get; }

        ICityRepository City { get; }

        ICountryRepository Country { get; }

        IVehicleManufacturerRepository VehicleManufacturer { get; }

        ICustomerRepository Customer { get; }

        ICustomerVehicleRepository CustomerVehicle { get; }

        IVehicleRepository Vehicle { get; }

        IVehicleTypeRepository VehicleType { get; }

        IReplacementPartManufacturerRepository ReplacementPartManufacturer { get; }

        IReplacementPartCategoryRepository ReplacementPartCategory { get; }

        IReplacementPartRepository ReplacementPart { get; }

        IRepairRepository Repair { get; }

        IRepairTypeRepository RepairType { get; }

        IEmployeeRepository Employee { get; }

        IUserProfileRepository UserProfile { get; }

        IOrderRepository Order { get; }

        IOperationStatus CommitAndGetStatus();
    }
}