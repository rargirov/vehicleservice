﻿using Blueprints.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Interfaces
{
    public interface IVehicleRepository : IRepository<Vehicle>
    {
        IEnumerable<Vehicle> FindAllByManufacturer(int manufacturerId);

        IQueryable<IGrouping<string, Vehicle>> GetGrouped(int vehicleTypeId, int manufacturerId = 0);
    }
}