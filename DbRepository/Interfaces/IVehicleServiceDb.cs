﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;

namespace DbRepository.Interfaces
{
    public interface IVehicleServiceDb : IDisposable
    {
        IQueryable<T> Entities<T>() where T : class;

        void Add<T>(T entity) where T : class;

        void Remove<T>(T entity) where T : class;

        void Update<T>(T entity, params string[] propertiesToUpdate) where T : class;

        int Save();
    }
}
