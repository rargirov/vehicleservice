﻿using Blueprints.DbModels;
using Blueprints.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Interfaces
{
    public interface IVehicleTypeRepository : IRepository<VehicleType>
    {
        VehicleTypeViewModel GetVehicleType(int vehicleTypeId, int manufacturerId = 0);
    }
}
