﻿using Blueprints.DbModels;
using Blueprints.Interfaces;
using Blueprints.Models;
using DbRepository.Interfaces;

using System.Collections.Generic;
using System.Linq;

namespace DbRepository.Repositories
{
    public class CityRepository : Repository<City>, ICityRepository
    {
        public CityRepository(IVehicleServiceDb context)
            : base(context)
        {
        }

        public IEnumerable<City> FindAllByCountry(int countryId)
        {
            var result = this.Find(x => x.CountryId == countryId).OrderBy(x => x.Name);
            return result;
        }
    }
}