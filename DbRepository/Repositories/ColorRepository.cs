﻿using Blueprints.DbModels;
using DbRepository.Interfaces;

namespace DbRepository.Repositories
{
    public class ColorRepository : Repository<Color>, IColorRepository
    {
        public ColorRepository(IVehicleServiceDb context)
            : base(context)
        {
        }
    }
}
