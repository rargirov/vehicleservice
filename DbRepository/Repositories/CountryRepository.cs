﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DbRepository.Repositories
{
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        public CountryRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }

        public IEnumerable<Country> FindAllWithCities()
        {
            return this.FindAll().Where(x => x.Cities.Count > 0);
        }
    }
}
