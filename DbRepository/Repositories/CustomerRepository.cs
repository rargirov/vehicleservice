﻿using Blueprints.DbModels;
using Blueprints.Models;
using DbRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DbRepository.Repositories
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(IVehicleServiceDb context)
            : base(context)
        {
        }
    }
}
