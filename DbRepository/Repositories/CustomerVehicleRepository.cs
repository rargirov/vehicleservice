﻿using Blueprints.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DbRepository.Interfaces;

namespace DbRepository.Repositories
{
    public class CustomerVehicleRepository : Repository<CustomerVehicle>, ICustomerVehicleRepository
    {
        public CustomerVehicleRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }
    }
}
