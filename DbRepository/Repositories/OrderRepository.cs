﻿using Blueprints.DbModels;
using DbRepository.Interfaces;

namespace DbRepository.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(IVehicleServiceDb db) 
            : base(db)
        {
        }
    }
}
