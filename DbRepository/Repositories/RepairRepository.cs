﻿using Blueprints.DbModels;
using Blueprints.Models;
using DbRepository.Helpers.LINQ;
using DbRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace DbRepository.Repositories
{
    public class RepairRepository : Repository<Repair>, IRepairRepository
    {
        public RepairRepository(IVehicleServiceDb context)
            : base(context)
        {
        }

        public IEnumerable<Repair> FindAllByCustomer(int customerId)
        {
            var repairs = this.Find(x => customerId == 0 || x.CustomerVehicle.Customer.Id == customerId)
                                .OrderBy(x => x.CustomerVehicle.Customer.FirstName)
                                .ToList();
            return repairs;
        }

        public IEnumerable<GroupResult> GetGroupedEntities(IEnumerable<Repair> repairs)
        {
            var grouped = repairs.GroupByMany(
                                x => x.CustomerVehicle.Customer.Name + " " + x.CustomerVehicle.Customer.Phone,
                                x => x.CustomerVehicle.Vehicle.Name + " " + x.CustomerVehicle.RegistrationNumber,
                                x => x.RepairType.Name + " (" + x.RepairType.PricePerHour + "/h)");                               

            return grouped;
        }
    }
}
