﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class ReplacementPartCategoryRepository : Repository<ReplacementPartCategory>, IReplacementPartCategoryRepository
    {
        public ReplacementPartCategoryRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }
    }
}
