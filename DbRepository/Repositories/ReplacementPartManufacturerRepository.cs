﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class ReplacementPartManufacturerRepository : Repository<ReplacementPartManufacturer>, IReplacementPartManufacturerRepository
    {
        public ReplacementPartManufacturerRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }
    }
}
