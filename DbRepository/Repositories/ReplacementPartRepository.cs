﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class ReplacementPartRepository : Repository<ReplacementPart>, IReplacementPartRepository
    {
        public ReplacementPartRepository(IVehicleServiceDb context)
            : base(context)
        {
        }

        public Dictionary<ReplacementPart, int> GetReplacementParts(string replacementPartIds)
        {
            var result = new Dictionary<ReplacementPart, int>();

            if (String.IsNullOrWhiteSpace(replacementPartIds) == false)
            {
                var groupedReplacementParts = replacementPartIds.Split(',').ToList().GroupBy(x => x);
                foreach (var group in groupedReplacementParts)
                {
                    int replacementPartId;
                    if (int.TryParse(group.Key, out replacementPartId))
                    {
                        var replacementPart = this.FindById(replacementPartId);
                        if (replacementPart != null)
                        {
                            result.Add(replacementPart, group.Count());
                        }
                    }
                }
            }

            return result;
        }
    }
}
