﻿using Blueprints.Interfaces;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DbRepository.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        private IVehicleServiceDb _dbContext;

        public Repository(IVehicleServiceDb dbContext)
        {
            _dbContext = dbContext;
        }

        public void Add(TEntity entity)
        {
            _dbContext.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            _dbContext.Remove(entity);
        }

        public void Update(TEntity entity, params string[] propsToUpdate)
        {
            _dbContext.Update(entity);
        }

        public TEntity FindById(int id)
        {
            return this.FindSingle(x => x.Id == id);
        }

        public TEntity FindSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Entities<TEntity>().SingleOrDefault(predicate);
        }

        public IEnumerable<TEntity> FindAll()
        {
            return _dbContext.Entities<TEntity>().ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Entities<TEntity>().Where(predicate);
        }
    }
}