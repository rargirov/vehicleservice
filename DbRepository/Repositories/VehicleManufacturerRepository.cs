﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class VehicleManufacturerRepository : Repository<VehicleManufacturer>, IVehicleManufacturerRepository
    {
        public VehicleManufacturerRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }

        public IEnumerable<IGrouping<string, VehicleManufacturer>> GetGroupedVehicleManufacturers()
        {
            return FindAll().OrderBy(x => x.Name).GroupBy(v => v.Country.Name).OrderBy(v => v.Key);
        }

        public IEnumerable<VehicleManufacturer> FindAllWithVehicles()
        {
            return this.Find(x => x.Vehicles.Count > 0).OrderBy(x => x.Name);
        }
    }
}
