﻿using Blueprints.DbModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class VehicleRepository : Repository<Vehicle>, IVehicleRepository
    {
        public VehicleRepository(IVehicleServiceDb context) 
            : base(context)
        {
        }

        public IEnumerable<Vehicle> FindAllByManufacturer(int manufacturerId)
        {
            return this.Find(x => x.VehicleManufacturerId == manufacturerId).OrderBy(x => x.Name);
        }

        public IQueryable<IGrouping<string, Vehicle>> GetGrouped(int vehicleTypeId, int manufacturerId = 0)
        {
            var model = this.FindAll()
                            .Where(x => x.VehicleTypeId == vehicleTypeId 
                                && (manufacturerId == 0 || x.VehicleManufacturerId == manufacturerId))
                            .OrderBy(x => x.Name).ToList()
                            .GroupBy(v => v.VehicleManufacturer.Name)
                            .OrderBy(v => v.Key).AsQueryable();
            return model;
        }
    }
}
