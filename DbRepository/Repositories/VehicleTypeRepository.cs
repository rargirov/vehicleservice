﻿using Blueprints.DbModels;
using Blueprints.ViewModels;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbRepository.Repositories
{
    public class VehicleTypeRepository : Repository<VehicleType>, IVehicleTypeRepository
    {
        public VehicleTypeRepository(IVehicleServiceDb context)
            : base(context)
        {
        }

        public VehicleTypeViewModel GetVehicleType(int vehicleTypeId, int manufacturerId = 0)
        {
            var model = this.FindAll()
                            .Select(x => 
                                new VehicleTypeViewModel()
                                {
                                    VehicleTypeId = x.Id,
                                    VehicleTypeName = x.Name,
                                    GroupedVehicles = x.Vehicles
                                                        .Where(y => manufacturerId == 0 || y.VehicleManufacturerId == manufacturerId)
                                                        .OrderBy(y => y.Name)
                                                        .GroupBy(y => y.VehicleManufacturer.Name)
                                                        .OrderBy(y => y.Key)
                                })
                            .FirstOrDefault(x => x.VehicleTypeId == vehicleTypeId);
            return model;
        }
    }
}