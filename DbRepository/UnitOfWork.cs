﻿using Blueprints.Interfaces;
using Blueprints.Models;
using DbRepository.Interfaces;
using DbRepository.Repositories;
using System;

namespace DbRepository
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private IVehicleServiceDb _db;

        private IColorRepository _color;
        private ICityRepository _city;
        private ICountryRepository _country;
        private IUserProfileRepository _userProfile;
        private IEmployeeRepository _employee;
        private ICustomerRepository _customer;
        private ICustomerVehicleRepository _customerVehicle;
        private IReplacementPartManufacturerRepository _replacementPartManufacturer;
        private IReplacementPartCategoryRepository _replacementPartCategory;
        private IReplacementPartRepository _replacementPart;
        private IRepairRepository _repair;
        private IRepairTypeRepository _repairType;
        private IOrderRepository _order;
        private IVehicleRepository _vehicle;
        private IVehicleManufacturerRepository _vehicleManufacturer;
        private IVehicleTypeRepository _vehicleType;
        
        public UnitOfWork(IVehicleServiceDb db)
        {
            _db = db;
        }


        public IColorRepository Color 
        {
            get
            {
                if (_color == null)
                {
                    _color = new ColorRepository(_db);
                }

                return _color;
            }
        }

        public ICityRepository City
        {
            get
            {
                if (_city == null)
                {
                    _city = new CityRepository(_db);
                }

                return _city;
            }
        }

        public ICountryRepository Country
        {
            get
            {
                if (_country == null)
                {
                    _country = new CountryRepository(_db);
                }

                return _country;
            } 
        }

        public ICustomerRepository Customer
        {
            get
            {
                if (_customer == null)
                {
                    _customer = new CustomerRepository(_db);
                }

                return _customer;
            } 
        }

        public ICustomerVehicleRepository CustomerVehicle
        {
            get
            {
                if (_customerVehicle == null)
                {
                    _customerVehicle = new CustomerVehicleRepository(_db);
                }

                return _customerVehicle;
            } 
        }

        public IVehicleManufacturerRepository VehicleManufacturer
        {
            get
            {
                if (_vehicleManufacturer == null)
                {
                    _vehicleManufacturer = new VehicleManufacturerRepository(_db);
                }

                return _vehicleManufacturer;
            } 
        }

        public IVehicleRepository Vehicle
        {
            get
            {
                if (_vehicle == null)
                {
                    _vehicle = new VehicleRepository(_db);
                }

                return _vehicle;
            } 
        }

        public IVehicleTypeRepository VehicleType
        {
            get
            {
                if (_vehicleType == null)
                {
                    _vehicleType = new VehicleTypeRepository(_db);
                }

                return _vehicleType;
            } 
        }

        public IReplacementPartManufacturerRepository ReplacementPartManufacturer
        {
            get
            {
                if (_replacementPartManufacturer == null)
                {
                    _replacementPartManufacturer = new ReplacementPartManufacturerRepository(_db);
                }

                return _replacementPartManufacturer;
            } 
        }

        public IReplacementPartCategoryRepository ReplacementPartCategory
        {
            get
            {
                if (_replacementPartCategory == null)
                {
                    _replacementPartCategory = new ReplacementPartCategoryRepository(_db);
                }

                return _replacementPartCategory;
            } 
        }

        public IReplacementPartRepository ReplacementPart
        {
            get
            {
                if (_replacementPart == null)
                {
                    _replacementPart = new ReplacementPartRepository(_db);
                }

                return _replacementPart;
            } 
        }

        public IRepairRepository Repair
        {
            get
            {
                if (_repair == null)
                {
                    _repair = new RepairRepository(_db);
                }

                return _repair;
            }  
        }

        public IRepairTypeRepository RepairType
        {
            get
            {
                if (_repairType == null)
                {
                    _repairType = new RepairTypeRepository(_db);
                }

                return _repairType;
            }  
        }

        public IEmployeeRepository Employee
        {
            get
            {
                if (_employee == null)
                {
                    _employee = new EmployeeRepository(_db);
                }

                return _employee;
            }  
        }

        public IUserProfileRepository UserProfile
        {
            get
            {
                if (_userProfile == null)
                {
                    _userProfile = new UserProfileRepository(_db);
                }

                return _userProfile;
            }  
        }

        public IOrderRepository Order
        {
            get
            {
                if (_order == null)
                {
                    _order = new OrderRepository(_db);
                }

                return _order;
            }   
        }


        public IOperationStatus CommitAndGetStatus()
        {
            var status = new OperationStatus { IsSuccessful = true };
            try
            {
                status.IsSuccessful = (_db.Save() > 0);
            }
            catch (Exception exp)
            {
                status = OperationStatus.CreateFromException("Error saving: ", exp);
            }

            return status;
        }

        public void Dispose()
        {
            if (_db != null)
            {
                _db.Dispose();
            }
        }
    }
}