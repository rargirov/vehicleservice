﻿using DbRepository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using VehicleService.Controllers;
using VehicleService.Models.Factories;
using VehicleService.Tests.Fakes;

namespace VehicleService.Tests.Controllers
{
    [TestClass]
    public class CountryControllerTest
    {
        [TestMethod]
        public void Index()
        {
            var db = new FakeVehicleServiceDb();
            db.AddSet(TestData.Countries);

            var uow = UowFactory.BuildUnitOfWork(db);

            // Arrange
            var controller = new CountryController(uow);
            //var entities = controller.Db.Country.GetEntities(controller.PagerSettings);

            // Act
            var result = controller.Index() as ViewResult;
            
            // Assert
            //Assert.AreEqual(result.Model, entities);
        }

        [TestMethod]
        public void Edit()
        {
            var db = new FakeVehicleServiceDb();
            db.AddSet(TestData.Countries);
            var uow = new UnitOfWork(db);
            // Arrange
            var controller = new CountryController(uow);

            // Act
            var result = controller.Edit() as ViewResult;

            // Assert
        }
    }
}
