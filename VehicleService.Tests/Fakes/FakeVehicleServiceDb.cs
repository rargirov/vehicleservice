﻿using Blueprints.Interfaces;
using DbRepository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace VehicleService.Tests.Fakes
{
    class FakeVehicleServiceDb : IVehicleServiceDb
    {
        public Dictionary<Type, object> Sets = new Dictionary<Type, object>();

        public void AddSet<T>(IQueryable<T> objects) where T : class
        {
            Sets.Add(typeof(T), objects);
        }

        public IEnumerable<T> Find<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return null;
        }

        public void Update<T>(T entity, params string[] propsToUpdate) where T : class
        {
            ;
        }

        public IEnumerable<T> GetAll<T>() where T : class
        {
            return null;
        }
        
        public T GetById<T>(int id) where T : class
        {
            return new Object() as T;
        }
        
        public void Add(IEntity entity)
        { }
        
        public void Add<T>(T entity) where T : class
        {
            //Sets.Add(typeof(T), entity);
        }

        public void Remove<T>(T entity) where T : class
        {
            ;
        }

        public int Save()
        {
            return 0;
        }

        public void Dispose()
        {
            ;
        }

        public IQueryable<T> Entities<T>() where T : class
        {
            throw new NotImplementedException();
        }
    }
}
