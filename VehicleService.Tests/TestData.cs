﻿using Blueprints.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleService.Tests
{
    class TestData
    {
        public static IQueryable<Country> Countries
        {
            get
            {
                var counties = new List<Country>();

                for (int i = 1; i < 1000; i++)
                {
                    var country = new Country
                    {
                        Name = "Test_Country_" + i,
                        Id = i,
                        Cities = TestData.Cities.ToList()
                    };

                    counties.Add(country);
                }

                return counties.AsQueryable();
            }
        }

        public static IQueryable<City> Cities
        {
            get
            {
                var counties = new List<City>();

                for (int i = 1; i <= 1000; i++)
                {
                    var country = new City
                    {
                        Id = i,
                        Name = "Test_City_" + i,
                        CountryId = i,
                    };

                    counties.Add(country);
                }

                return counties.AsQueryable();
            }
        }
    }
}
