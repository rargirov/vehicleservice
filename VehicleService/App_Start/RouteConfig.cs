﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VehicleService.Models;

namespace VehicleService
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "VehicleType",
                url: "{controller}/{action}/{id}/{manufacturerId}",
                defaults: new
                {
                    controller = "VehicleType",
                    action = DefaultActionNames.Details,
                }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new
                {
                    controller = Constants.HomeControllerName,
                    action = DefaultActionNames.Index,
                    id = UrlParameter.Optional
                }
            );
        }
    }
}