﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace VehicleService
{
    public static class SecurityConfig
    {
        public static void RegisterDatabaseConnection()
        {
            if (WebSecurity.Initialized == false)
            {
                WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "Id", "UserName", autoCreateTables: true);
            }
        }
    }
}