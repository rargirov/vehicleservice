﻿using Blueprints.Interfaces;
using DbRepository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace VehicleService.Controllers
{
    public abstract class BaseController : Controller
    {
        protected virtual int EntitiesPerPage
        {
            get
            {
                return 1;
            }
        }

        protected virtual int EntitiesInAutocomplete
        {
            get
            {
                return 5;
            }
        }

        protected SelectList BuildSelectList<T>(IRepository<T> repository, int? selectedValue = null) where T : class, IBaseEntity 
        {
            var entities = repository.FindAll().OrderBy(x => x.Name);
            return this.BuildSelectList(entities, selectedValue);
        }

        protected SelectList BuildSelectList<T>(IEnumerable<T> entities, int? selectedValue = null) where T : class, IBaseEntity
        {
            return new SelectList(entities, "Id", "Name", selectedValue);
        }
    }
}