﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using PagedList;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class CityController : BaseController
    {
        private ICityRepository _repository;

        public CityController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public CityController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        private IUnitOfWork UnitOfWork { get; set; }

        private ICityRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.City;
                }

                return _repository;
            }
        }

        public ActionResult Autocomplete(string term)
        {
            var searchTerm = (term ?? "").Trim().ToLower();
            var model = this.Repository
                                .Find(x => x.Name.ToLower().StartsWith(searchTerm))
                                .Take(this.EntitiesInAutocomplete)
                                .Select(x => new
                                {
                                    label = x.Name
                                });

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListByCountry(int countryId)
        {
            if (Request.IsAjaxRequest())
            {
                var model = Repository.FindAllByCountry(countryId)
                                        .Select(x => new
                                        {
                                            Text = x.Name,
                                            Value = x.Id
                                        });
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            return HttpNotFound();
        }

        public ActionResult ListAjax(string searchTerm, int pageIndex = 1)
        {
            if (Request.IsAjaxRequest())
            {
                searchTerm = (searchTerm ?? "").Trim();
                var model = Repository.Find(x => searchTerm == "" || x.Name.StartsWith(searchTerm))
                                       .OrderBy(x => x.Name)
                                       .ToPagedList(pageIndex, this.EntitiesPerPage);
                return PartialView("_List", model);
            }

            return HttpNotFound();
        }

        public ActionResult Index()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuCreate();

            var model = Repository.FindAll()
                                    .OrderBy(x => x.Name)
                                    .ToPagedList(1, this.EntitiesPerPage);
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
            ViewBag.CountryId = this.BuildSelectList(UnitOfWork.Country);
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });
                ViewBag.CountryId = this.BuildSelectList(UnitOfWork.Country, model.CountryId);
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDelete(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(City model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            ViewBag.CountryId = this.BuildSelectList(UnitOfWork.Country);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(City model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    Repository.Remove(model);

                    var status = UnitOfWork.CommitAndGetStatus();
                    if (status.IsSuccessful)
                    {
                        return RedirectToAction(DefaultActionNames.Index);
                    }
                    else
                    {
                        //logging
                    }
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}