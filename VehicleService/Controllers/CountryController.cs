﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;
using System.Linq;
using PagedList;

namespace VehicleService.Controllers
{
    public class CountryController : BaseController
    {
        private ICountryRepository _repository;


        public CountryController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public CountryController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        private IUnitOfWork UnitOfWork { get; set; }

        private ICountryRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.Country;
                }

                return _repository;
            }
        }

        public ActionResult Autocomplete(string term)
        {
            var searchTerm = (term ?? "").Trim().ToLower();
            var model = Repository.Find(x => x.Name.ToLower().StartsWith(searchTerm))
                                .Take(5)
                                .Select(x => new
                                {
                                    label = x.Name
                                });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListAjax(string searchTerm, int pageIndex)
        {
            if (Request.IsAjaxRequest())
            {
                searchTerm = (searchTerm ?? "").Trim();
                var model = Repository.Find(x => searchTerm == "" || x.Name.StartsWith(searchTerm))
                                        .OrderBy(x => x.Name)
                                        .ToPagedList(pageIndex, this.EntitiesPerPage);

                return PartialView("_List", model);
            }

            return HttpNotFound();
        }

        public ActionResult Index()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuCreate();
            var model = Repository.FindAll()
                                    .OrderBy(x => x.Name)
                                    .ToPagedList(1, this.EntitiesPerPage);
            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDetails(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Create()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDelete(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Country model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Country model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                Repository.Remove(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}