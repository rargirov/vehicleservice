﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class CustomerVehicleController : BaseController
    {
        private ICustomerVehicleRepository _repository;


        public CustomerVehicleController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public CustomerVehicleController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        private IUnitOfWork UnitOfWork { get; set; }

        private ICustomerVehicleRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.CustomerVehicle;
                }

                return _repository;
            }
        }

        public ActionResult Index([Bind(Prefix = "id")] int customerId = 0)
        {
            return RedirectToAction(DefaultActionNames.Details, "Customer", new { id = customerId });
        }

        public ActionResult Create([Bind(Prefix = "id")] int customerId = 0)
        {
            var customer = UnitOfWork.Customer.FindById(customerId);
            if (customer != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuIndex(new { id = customer.Id });
                ViewBag.Customer = customer;

                var vehicleManufacturersWithVehicles = UnitOfWork.VehicleManufacturer.FindAllWithVehicles();
                ViewBag.VehicleManufacturerId = this.BuildSelectList(vehicleManufacturersWithVehicles);
                ViewBag.VehicleId = Enumerable.Empty<SelectListItem>();
                ViewBag.ColorId = this.BuildSelectList(UnitOfWork.Color);
                return View();
            }

            return HttpNotFound();
        }

        public ActionResult Details(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenu
                (
                    MenuFactory.MenuItemCreate(new { id = model.Customer.Id }),
                    MenuFactory.MenuItemEdit(new { id = model.Id }),
                    MenuFactory.MenuItemDelete(new { id = model.Id }),
                    MenuFactory.MenuItemIndex(new { id = model.Customer.Id })
                );
               
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenu
                (
                    MenuFactory.MenuItemCreate(new { id = model.PersonId }),
                    MenuFactory.MenuItemDelete(new { id = model.Id }),
                    MenuFactory.MenuItemIndex(new { id = model.PersonId })
                );

                var vehicleManufacturers = UnitOfWork.VehicleManufacturer.FindAllWithVehicles();
                ViewBag.VehicleManufacturerId = this.BuildSelectList(vehicleManufacturers, model.Vehicle.VehicleManufacturerId);

                ViewBag.VehicleId = this.BuildSelectList(UnitOfWork.Vehicle, model.VehicleId);
                ViewBag.ColorId = this.BuildSelectList(UnitOfWork.Color, model.ColorId);

                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuIndex(new { id = model.Customer.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerVehicle model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index, new { id = model.PersonId });
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            var customer = UnitOfWork.Customer.FindById(model.PersonId);
            if (customer != null)
            {
                ViewBag.Customer = customer;
                ViewBag.VehicleId = this.BuildSelectList(UnitOfWork.Vehicle);
                ViewBag.ColorId = this.BuildSelectList(UnitOfWork.Color);
                return View();
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerVehicle model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index, new { id = model.PersonId });
                }
                else
                {
                    //logging
                }
            }

            ViewBag.VehicleId = this.BuildSelectList(UnitOfWork.Vehicle);
            ViewBag.ColorId = this.BuildSelectList(UnitOfWork.Color);
            var cleanModel = Repository.FindById(model.Id);
            return View(cleanModel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    Repository.Remove(model);

                    var status = UnitOfWork.CommitAndGetStatus();
                    if (status.IsSuccessful)
                    {
                        return RedirectToAction(DefaultActionNames.Index, new { id = model.PersonId });
                    }
                    else
                    {
                        //logging
                    }

                    return View(model);
                }

                return View(model);

            }

            return HttpNotFound();
        }
    }
}