﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class EmployeeController : BaseController
    {
        private IEmployeeRepository _repository;


        public EmployeeController() 
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public EmployeeController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }


        private IUnitOfWork UnitOfWork { get; set; }


        private IEmployeeRepository Repository
        { 
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.Employee;
                }

                return _repository; 
            } 
        }


        public ActionResult Index()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuCreate();
            var model = Repository.FindAll().OrderBy(x => x.Name);
            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDetails(new { id = model.Id });    
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Create()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();

            var countries = UnitOfWork.Country.FindAllWithCities();
            ViewBag.CountryId = this.BuildSelectList(countries);

            ViewBag.CityId = Enumerable.Empty<SelectListItem>();
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });

                var countries = UnitOfWork.Country.FindAllWithCities();
                ViewBag.CountryId = this.BuildSelectList(countries, model.City.CountryId);

                var cities = UnitOfWork.City.FindAllByCountry(model.City.CountryId);
                ViewBag.CityId = this.BuildSelectList(cities, model.CityId);

                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDelete(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Employee model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            ViewBag.CityId = this.BuildSelectList(UnitOfWork.City);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Employee model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            ViewBag.CityId = this.BuildSelectList(UnitOfWork.City, model.CityId);
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = UnitOfWork.Employee.FindById(id);
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    Repository.Remove(model);

                    var status = UnitOfWork.CommitAndGetStatus();
                    if (status.IsSuccessful)
                    {
                        return RedirectToAction(DefaultActionNames.Index);
                    }
                    else
                    {
                        //logging
                    }
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}