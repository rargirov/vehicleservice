﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class OrderController : BaseController
    {
        private IOrderRepository _repository;


        public OrderController() 
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public OrderController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }


        private IUnitOfWork UnitOfWork { get; set; }

        private IOrderRepository Repository
        { 
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.Order;
                }

                return _repository; 
            } 
        }
        
        
        public ActionResult Index()
        {
            var model = Repository.FindAll().OrderByDescending(x => x.CreatedOn);
            return View(model);
        }

        public ActionResult Create([Bind(Prefix = "id")] int repairId)
        {
            var model = Repository.FindSingle(x => x.RepairId == repairId);
            if (model != null)
            {
                return RedirectToAction("Edit", new { id = repairId });
            }

            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
            ViewBag.RepairId = repairId;
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                var subMenu = MenuFactory.BuildMenu();
                var repairMenuItem = MenuFactory.BuildMenuItem(
                    "Details",
                    DefaultActionNames.Details,
                    "Repair",
                    new { id = model.Repair.Id }
                );
                subMenu.Add(repairMenuItem);
                subMenu.Add(MenuFactory.MenuItemDelete(new { id = model.Id }));
                subMenu.Add(MenuFactory.MenuItemIndex());
                ViewBag.SubMenu = subMenu; 
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order model)
        {
            if (ModelState.IsValid)
            {
                var repair = UnitOfWork.Repair.FindById(model.RepairId);
                model.TotalPrice = PriceCalculator.CalculateOrderTotalPrice(model.AdditionalPrice, repair);

                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order model)
        {
            if (ModelState.IsValid)
            {
                var repair = UnitOfWork.Repair.FindById(model.RepairId);
                model.TotalPrice = PriceCalculator.CalculateOrderTotalPrice(model.AdditionalPrice, repair);

                Repository.Update(model, "IsPaid", "AdditionPrice", "TotalPrice");

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            var zmodel = Repository.FindById(model.Id);
            return View(zmodel);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                Repository.Remove(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}