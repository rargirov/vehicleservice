﻿using Blueprints.DbModels;
using Blueprints.ViewModels;
using DbRepository;
using DbRepository.Interfaces;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class RepairController : BaseController
    {
        private IRepairRepository _repository;


        public RepairController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public RepairController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        protected override int EntitiesPerPage
        {
            get
            {
                return 1;
            }
        }

        private IUnitOfWork UnitOfWork { get; set; }

        private IRepairRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.Repair;
                }

                return _repository;
            }
        }


        public ActionResult ListAjax(int pageIndex)
        {
            if (Request.IsAjaxRequest())
            {
                var repairs = Repository.FindAll();
                var model = Repository.GetGroupedEntities(repairs).ToPagedList(pageIndex, this.EntitiesPerPage);
                ViewBag.CustomerVehicleVehicleName = repairs.FirstOrDefault().CustomerVehicle.Vehicle.Name;
                return PartialView("_CustomersRepairs", model);
            }

            return HttpNotFound();
        }

        public ActionResult Index([Bind(Prefix = "id")] int customerId = 0)
        {
            var repairs = Repository.FindAllByCustomer(customerId);
            var model = Repository.GetGroupedEntities(repairs).ToPagedList(1, this.EntitiesPerPage);
            ViewBag.CustomerVehicleVehicleName = repairs.FirstOrDefault().CustomerVehicle.Vehicle.Name;
            return View(model);
        }

        public ActionResult Vehicle([Bind(Prefix = "id")] int customerVehicleId)
        {
            var repairs = Repository.Find(x => x.CustomerVehicle.Id == customerVehicleId).ToList();
            if (repairs.Any())
            {
                var model = Repository.GetGroupedEntities(repairs).SingleOrDefault();
                ViewBag.CustomerVehicleVehicleName = repairs.FirstOrDefault().CustomerVehicle.Vehicle.Name;

                return View(model);
            }

            return RedirectToAction("Index");
        }

        public ActionResult Details(int id = 0)
        {
            var repair = Repository.FindById(id);
            if (repair != null)
            {
                var order = UnitOfWork.Order.FindSingle(x => x.RepairId == repair.Id);

                var routeValues = new { id = repair.Id };

                var subMenu = MenuFactory.BuildMenu();
                if (repair.IsCompleted == false)
                {
                    subMenu.Add(MenuFactory.MenuItemEdit(routeValues));
                }
                else
                {
                    if (order == null)
                    {
                        var orderMenuItem = MenuFactory.BuildMenuItem
                        (
                            "Create Order",
                            DefaultActionNames.Create,
                            "Order",
                            routeValues
                        );
                        subMenu.Add(orderMenuItem);
                    }
                }
                subMenu.Add(MenuFactory.MenuItemDelete(routeValues));
                subMenu.Add(MenuFactory.MenuItemIndex());

                ViewBag.SubMenu = subMenu;

                var model = new RepairViewModel(repair, order);
                model.ReplacementParts = UnitOfWork.ReplacementPart.GetReplacementParts(repair.ReplacementPartIds);
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Create([Bind(Prefix = "id")] int customerVehicleId)
        {
            ViewBag.CustomerVehicle = UnitOfWork.CustomerVehicle.FindById(customerVehicleId);
            ViewBag.PersonId = this.BuildSelectList(UnitOfWork.Employee);
            ViewBag.RepairTypeId = this.BuildSelectList(UnitOfWork.RepairType);
            return View();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });

                var subMenu = MenuFactory.BuildMenu();
                subMenu.Add(MenuFactory.MenuItemDelete(new { id = model.Id }));
                subMenu.Add(MenuFactory.MenuItemIndex());
                ViewBag.SubMenu = subMenu; 


                ViewBag.PersonId = this.BuildSelectList(UnitOfWork.Employee, model.PersonId);
                ViewBag.RepairTypeId = this.BuildSelectList(UnitOfWork.RepairType, model.RepairTypeId);
                var replacementParts = UnitOfWork.ReplacementPart.FindAll().OrderBy(x => x.Name);
                ViewBag.ReplacementPartIds = this.GetReplacementParts(replacementParts, model.ReplacementPartIds);
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Repair model)
        {
            if (ModelState.IsValid)
            {
                var repairType = UnitOfWork.RepairType.FindById(model.RepairTypeId);
                model.LaborPrice = PriceCalculator.CalculateLaborPrice(model.WorkingHours, repairType.PricePerHour);
                
                var rparts = UnitOfWork.ReplacementPart.GetReplacementParts(model.ReplacementPartIds);
                model.ReplacementPartsSum = PriceCalculator.CalculateReplacementPartsPrice(rparts);

                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction("Vehicle", new { id = model.CustomerVehicleId });
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            ViewBag.CustomerVehicleId = model.CustomerVehicleId;
            ViewBag.PersonId = this.BuildSelectList(UnitOfWork.Employee);
            ViewBag.RepairTypeId = this.BuildSelectList(UnitOfWork.RepairType, model.RepairTypeId);
            var replacementParts = UnitOfWork.ReplacementPart.FindAll().OrderBy(x => x.Name);
            ViewBag.ReplacementPartIds = this.GetReplacementParts(replacementParts);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Repair model)
        {
            if (ModelState.IsValid)
            {
                var repairType = UnitOfWork.RepairType.FindById(model.RepairTypeId);
                model.LaborPrice = PriceCalculator.CalculateLaborPrice(model.WorkingHours, repairType.PricePerHour);

                var rparts = UnitOfWork.ReplacementPart.GetReplacementParts(model.ReplacementPartIds);
                model.ReplacementPartsSum = PriceCalculator.CalculateReplacementPartsPrice(rparts);
                
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction("Vehicle", new { id = model.CustomerVehicleId });
                }
                else
                {
                    //logging
                }
            }

            ViewBag.PersonId = this.BuildSelectList(UnitOfWork.Employee, model.PersonId);
            ViewBag.RepairTypeId = this.BuildSelectList(UnitOfWork.RepairType, model.RepairTypeId);
            var replacementParts = UnitOfWork.ReplacementPart.FindAll().OrderBy(x => x.Name);
            ViewBag.ReplacementPartIds = this.GetReplacementParts(replacementParts);
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    Repository.Remove(model);

                    var status = UnitOfWork.CommitAndGetStatus();
                    if (status.IsSuccessful)
                    {
                        return RedirectToAction("Vehicle", new { id = model.CustomerVehicleId });
                    }
                    else
                    {
                        //logging
                    }

                    return View(model);
                }

                return View(model);
            }

            return View(model);
        }

        private List<SelectListItem> GetReplacementParts(IEnumerable<ReplacementPart> replacementParts, string selectedValues = "")
        {
            var listSelectListItems = new List<SelectListItem>();

            var replacementPartIds = new List<string>();
            if (String.IsNullOrWhiteSpace(selectedValues) == false)
            {
                replacementPartIds = selectedValues.Split(',').ToList();
            }

            foreach (var replacementPart in replacementParts)
            {
                var isSelected = false;
                if (replacementPartIds.Any())
                {
                    isSelected = replacementPartIds.Contains(replacementPart.Id.ToString());                    
                }

                SelectListItem selectList = new SelectListItem()
                {
                    Text = replacementPart.Name,
                    Value = replacementPart.Id.ToString(),
                    Selected = isSelected
                };

                listSelectListItems.Add(selectList);
            }

            return listSelectListItems;
        }
    }
}