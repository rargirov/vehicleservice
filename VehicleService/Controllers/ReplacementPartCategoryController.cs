﻿using Blueprints.DbModels;
using DbRepository;
using DbRepository.Interfaces;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class ReplacementPartCategoryController : BaseController
    {
        private IReplacementPartCategoryRepository _repository;


        public ReplacementPartCategoryController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public ReplacementPartCategoryController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }


        private IUnitOfWork UnitOfWork { get; set; }

        private IReplacementPartCategoryRepository Repository
        { 
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.ReplacementPartCategory;
                }

                return _repository; 
            } 
        }


        public ActionResult Index()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuCreate();
            var model = Repository.FindAll().OrderBy(x => x.Name);
            return View(model);
        }

        public ActionResult Create()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
            return View();
        }

        public ActionResult Details(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDetails(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDelete(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ReplacementPartCategory model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ReplacementPartCategory model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                if (ModelState.IsValid)
                {
                    Repository.Remove(model);

                    var status = UnitOfWork.CommitAndGetStatus();
                    if (status.IsSuccessful)
                    {
                        return RedirectToAction(DefaultActionNames.Index);
                    }
                    else
                    {
                        //logging
                    }

                    return View(model);
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}