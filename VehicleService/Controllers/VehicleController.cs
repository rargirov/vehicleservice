﻿using Blueprints.DbModels;
using Blueprints.Models;
using DbRepository;
using DbRepository.Interfaces;
using System.Linq;
using System.Web.Mvc;
using VehicleService.Models;
using VehicleService.Models.Factories;

namespace VehicleService.Controllers
{
    public class VehicleController : BaseController
    {
        private IVehicleRepository _repository;


        public VehicleController()
        {
            this.UnitOfWork = UowFactory.BuildUnitOfWork();
        }

        public VehicleController(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        private IUnitOfWork UnitOfWork { get; set; }

        private IVehicleRepository Repository
        {
            get
            {
                if (_repository == null)
                {
                    _repository = this.UnitOfWork.Vehicle;
                }

                return _repository;
            }
        }


        public ActionResult ListByManufacturer(int manufacturerId)
        {
            if (Request.IsAjaxRequest())
            {
                var model = Repository.FindAllByManufacturer(manufacturerId)
                                        .Select(x => new
                                        {
                                            Text = x.Name,
                                            Value = x.Id.ToString()
                                        });
                return Json(model, JsonRequestBehavior.AllowGet);
            }

            return HttpNotFound();
        }
        public ActionResult Index()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuCreate();
            var model = Repository.FindAll().OrderBy(x => x.VehicleModel);
            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDetails(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }

        public ActionResult Create()
        {
            ViewBag.SubMenu = MenuFactory.BuildMenuIndex();
            ViewBag.VehicleManufacturerId = this.BuildSelectList(UnitOfWork.VehicleManufacturer);
            ViewBag.VehicleTypeId = this.BuildSelectList(UnitOfWork.VehicleType);
            return View();
        }
        
        public ActionResult Edit(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuEdit(new { id = model.Id });
                ViewBag.VehicleManufacturerId = this.BuildSelectList(UnitOfWork.VehicleManufacturer, model.VehicleManufacturerId);
                ViewBag.VehicleTypeId = this.BuildSelectList(UnitOfWork.VehicleType, model.VehicleTypeId);
                return View(model);
            }

            return HttpNotFound();
        }
        
        public ActionResult Delete(int id = 0)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                ViewBag.SubMenu = MenuFactory.BuildMenuDelete(new { id = model.Id });
                return View(model);
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vehicle model)
        {
            if (ModelState.IsValid)
            {
                Repository.Add(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logged error ILogger TODO
                }
            }

            ViewBag.VehicleManufacturerId = this.BuildSelectList(UnitOfWork.VehicleManufacturer, model.VehicleManufacturerId);
            ViewBag.VehicleTypeId = this.BuildSelectList(UnitOfWork.VehicleType, model.VehicleTypeId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Vehicle model)
        {
            if (ModelState.IsValid)
            {
                Repository.Update(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }
            }

            ViewBag.VehicleManufacturerId = this.BuildSelectList(UnitOfWork.VehicleManufacturer, model.VehicleManufacturerId);
            ViewBag.VehicleTypeId = this.BuildSelectList(UnitOfWork.VehicleType, model.VehicleTypeId);
            return View(model);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var model = Repository.FindById(id);
            if (model != null)
            {
                Repository.Remove(model);

                var status = UnitOfWork.CommitAndGetStatus();
                if (status.IsSuccessful)
                {
                    return RedirectToAction(DefaultActionNames.Index);
                }
                else
                {
                    //logging
                }

                return View(model);
            }

            return HttpNotFound();
        }
    }
}