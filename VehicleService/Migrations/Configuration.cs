using Blueprints.DbModels;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Security;
using VehicleService.Models.DB;
using WebMatrix.WebData;

namespace VehicleService.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<VehicleServiceDb>
    {
        bool _isRealData = true;
        VehicleServiceDb _context;

        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(VehicleServiceDb context)
        {
            _context = context;
            SeedMembership();
            SeedTestData();
        }

        private void SeedMembership()
        {
            SecurityConfig.RegisterDatabaseConnection();

            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            if (roles.RoleExists("Admin") == false)
            {
                roles.CreateRole("Admin");
            }

            if (membership.GetUser("admin", false) == null)
            {
                membership.CreateUserAndAccount("admin", "blqblqblq");
            }

            if (roles.GetRolesForUser("admin").Contains("Admin") == false)
            {
                roles.AddUsersToRoles(new[] { "admin" }, new[] { "admin" });
            }
        }

        private void SeedTestData()
        {
            AddColors();
            AddCountries();
            AddCities();
            AddRepairTypes();
            AddVehicleTypes();
            AddVehicleManufacturers();
            AddReplacementPartManufacturers();
            AddReplacementPartCategories();
            AddCustomers();
            AddEmplyees();
            AddVehicles();
            AddReplacementParts();
            AddCustomerVehicles();
            AddRepairs();
        }

        private void AddColors()
        {
            if (_isRealData)
            {
                _context.Colors.AddOrUpdate(
                x => x.Name,
                    new Color { Id = 1, Name = "Black" },
                    new Color { Id = 2, Name = "White" },
                    new Color { Id = 3, Name = "Red" },
                    new Color { Id = 4, Name = "Blue" },
                    new Color { Id = 5, Name = "Green" },
                    new Color { Id = 6, Name = "Grey" },
                    new Color { Id = 7, Name = "Purple" },
                    new Color { Id = 8, Name = "Dark blue" },
                    new Color { Id = 9, Name = "Dark red" },
                    new Color { Id = 10, Name = "Dark green" }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.Colors.AddOrUpdate(c => c.Name,
                        new Color { Id = i, Name = "Color_" + i.ToString() });
                }
            }
        }

        private void AddCountries()
        {
            if (_isRealData)
            {
                _context.Countries.AddOrUpdate(
                x => x.Name,
                    new Country { Id = 1, Name = "Bulgaria" },
                    new Country { Id = 2, Name = "Other" },
                    new Country { Id = 3, Name = "Greece" },
                    new Country { Id = 4, Name = "England" },
                    new Country { Id = 5, Name = "Turkey" },
                    new Country { Id = 6, Name = "France" },
                    new Country { Id = 7, Name = "Norway" },
                    new Country { Id = 8, Name = "Spain" },
                    new Country { Id = 9, Name = "Russia" },
                    new Country { Id = 10, Name = "Japan" },
                    new Country { Id = 11, Name = "Germany" },
                    new Country { Id = 12, Name = "USA" },
                    new Country { Id = 13, Name = "Italia" }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.Countries.AddOrUpdate(c => c.Name,
                        new Country { Id = i, Name = "Country_" + i.ToString() });
                }
            }
        }

        private void AddCities()
        {
            if (_isRealData)
            {
                _context.Cities.AddOrUpdate(
                    x => x.Name,
                    new City { Id = 1, Name = "Plovdiv", CountryId = 1 },
                    new City { Id = 2, Name = "Sofiq", CountryId = 1 },
                    new City { Id = 3, Name = "Ruse", CountryId = 1 },
                    new City { Id = 4, Name = "Burgas", CountryId = 1 },
                    new City { Id = 5, Name = "Ankara", CountryId = 5 },
                    new City { Id = 6, Name = "London", CountryId = 4 },
                    new City { Id = 7, Name = "Frankfurt", CountryId = 11 },
                    new City { Id = 8, Name = "Paris", CountryId = 6 },
                    new City { Id = 9, Name = "Malaga", CountryId = 8 },
                    new City { Id = 10, Name = "Madrid", CountryId = 8 },
                    new City { Id = 11, Name = "Oslo", CountryId = 7 },
                    new City { Id = 12, Name = "Nica", CountryId = 6 },
                    new City { Id = 13, Name = "Other", CountryId = 2 },
                    new City { Id = 14, Name = "Atina", CountryId = 3 }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.Cities.AddOrUpdate(x => x.Name,
                        new City { Id = i, Name = "City_" + i.ToString() });
                }
            }
        }

        private void AddRepairTypes()
        {
            if (_isRealData)
            {
                _context.RepairTypes.AddOrUpdate(
                   x => x.Name,
                   new RepairType { Id = 1, Name = "Custom", PricePerHour = 11 },
                   new RepairType { Id = 2, Name = "Clutch change", PricePerHour = 10 },
                   new RepairType { Id = 3, Name = "Brake lining change", PricePerHour = 110 },
                   new RepairType { Id = 4, Name = "Brake disks change", PricePerHour = 220 },
                   new RepairType { Id = 5, Name = "Suspension", PricePerHour = 330 },
                   new RepairType { Id = 6, Name = "Cleaning", PricePerHour = 440 },
                   new RepairType { Id = 7, Name = "Oil change", PricePerHour = 550 }
               );   
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.RepairTypes.AddOrUpdate(x => x.Name,
                        new RepairType { Id = i, Name = "RepairType_" + i.ToString(), PricePerHour = i + i });
                }
            }
        }

        private void AddVehicleTypes()
        {
            if (_isRealData)
            {
                _context.VehicleTypes.AddOrUpdate(
                    x => x.Name,
                    new VehicleType { Id = 1, Name = "Car" },
                    new VehicleType { Id = 2, Name = "Microbus" },
                    new VehicleType { Id = 3, Name = "Motor" },
                    new VehicleType { Id = 4, Name = "Track" }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.VehicleTypes.AddOrUpdate(x => x.Name,
                        new VehicleType { Id = i, Name = "VehicleType_" + i.ToString(), });
                }
            }
        }

        private void AddVehicleManufacturers()
        {
            if (_isRealData)
            {
                _context.VehicleManufacturers.AddOrUpdate(
                    x => x.Name,
                    new VehicleManufacturer { Id = 1, Name = "BMW", CountryId = 11 },
                    new VehicleManufacturer { Id = 2, Name = "VW", CountryId = 11 },
                    new VehicleManufacturer { Id = 3, Name = "Opel", CountryId = 11 },
                    new VehicleManufacturer { Id = 4, Name = "Lada", CountryId = 9 },
                    new VehicleManufacturer { Id = 5, Name = "Suzuki", CountryId = 10 },
                    new VehicleManufacturer { Id = 6, Name = "Porche", CountryId = 11 },
                    new VehicleManufacturer { Id = 7, Name = "Mercedes", CountryId = 11 },
                    new VehicleManufacturer { Id = 8, Name = "Ford", CountryId = 12 },
                    new VehicleManufacturer { Id = 9, Name = "Honda", CountryId = 10 },
                    new VehicleManufacturer { Id = 10, Name = "Reno", CountryId = 6 },
                    new VehicleManufacturer { Id = 11, Name = "Fiat", CountryId = 13 }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.VehicleManufacturers.AddOrUpdate(
                        x => x.Name,
                        new VehicleManufacturer
                        {
                            Id = i,
                            Name = "VehicleManufacturer_" + i.ToString(),
                            CountryId = i,
                        });
                }
            }
        }

        private void AddReplacementPartManufacturers()
        {
            if (_isRealData)
            {
                _context.ReplacementPartManufacturers.AddOrUpdate(
                    x => x.Name,
                    new ReplacementPartManufacturer { Id = 1, Name = "MOOG", CountryId = 11, },
                    new ReplacementPartManufacturer { Id = 2, Name = "VAICO", CountryId = 11, },
                    new ReplacementPartManufacturer { Id = 3, Name = "BOSCH", CountryId = 11, },
                    new ReplacementPartManufacturer { Id = 4, Name = "BARUM", CountryId = 11, },
                    new ReplacementPartManufacturer { Id = 5, Name = "STARLINE", CountryId = 3, },
                    new ReplacementPartManufacturer { Id = 6, Name = "CIFAM", CountryId = 11, },
                    new ReplacementPartManufacturer { Id = 7, Name = "REMKAFLEX", CountryId = 11, }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.ReplacementPartManufacturers.AddOrUpdate(
                        x => x.Name,
                        new ReplacementPartManufacturer
                        {
                            Id = i,
                            Name = "ReplacementPartManufacturer_" + i.ToString(),
                            CountryId = i,
                        }
                    );
                }
            }
        }

        private void AddReplacementPartCategories()
        {
            if (_isRealData)
            {
                _context.ReplacementPartCategories.AddOrUpdate(
                    x => x.Name,
                    new ReplacementPartCategory { Id = 1, Name = "Other", },
                    new ReplacementPartCategory { Id = 2, Name = "Electric system", },
                    new ReplacementPartCategory { Id = 3, Name = "Break system", },
                    new ReplacementPartCategory { Id = 4, Name = "Engine", },
                    new ReplacementPartCategory { Id = 5, Name = "Exhaust", },
                    new ReplacementPartCategory { Id = 6, Name = "Steering system", },
                    new ReplacementPartCategory { Id = 7, Name = "Suspension", },
                    new ReplacementPartCategory { Id = 8, Name = "Cooling system", },
                    new ReplacementPartCategory { Id = 9, Name = "Fuel system", },
                    new ReplacementPartCategory { Id = 10, Name = "Ignition system", }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    _context.ReplacementPartCategories.AddOrUpdate(
                        x => x.Name,
                        new ReplacementPartCategory
                        {
                            Id = i,
                            Name = "ReplacementPartCategory_" + i.ToString(),
                        }
                    );
                }
            }
        }

        private void AddCustomers()
        {
            if (_isRealData)
            {
                _context.People.AddOrUpdate(
                    x => x.Phone,
                    new Customer
                    {
                        FirstName = "Ivan",
                        LastName = "Ivanov",
                        Company = "Super-Company!",
                        Phone = "0886334455",
                        Street = "Nqkwa ulica",
                        CityId = 1
                    },
                    new Customer
                    {
                        FirstName = "Ivan",
                        LastName = "Ivanov",
                        Company = "Super-Company!",
                        Phone = "099999999",
                        Street = "Nqkwa ulica",
                        CityId = 1
                    },
                    new Customer
                    {
                        FirstName = "Peter",
                        LastName = "Stoev",
                        Phone = "0888002212",
                        Street = "Druga ulica",
                        CityId = 14
                    },
                    new Customer
                    {
                        FirstName = "Georgi",
                        LastName = "Todorov",
                        Phone = "099872212",
                        Street = "Treta ulica",
                        CityId = 2
                    },
                    new Customer
                    {
                        FirstName = "Todor",
                        LastName = "Angelov",
                        Phone = "0998222212",
                        Street = "peta ulica",
                        CityId = 2
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.Customers.AddOrUpdate(
                        c => c.Phone,
                        new Customer
                        {
                            Id = i,
                            FirstName = "FirstName_" + index,
                            LastName = "LastName_" + index,
                            Company = "Company_" + index,
                            Phone = "088888888_" + index,
                            Street = "Street_" + index,
                            CityId = 1
                        }
                    );
                }
            }
        }

        private void AddEmplyees()
        {
            if (_isRealData)
            {
                _context.People.AddOrUpdate(
                    x => x.Phone,
                    new Employee
                    {
                        FirstName = "Pavlin",
                        LastName = "Petrov",
                        Phone = "07777777",
                        Street = "peta ulica",
                        CityId = 1,
                        Salary = 2000
                    },
                    new Employee
                    {
                        FirstName = "Petko",
                        LastName = "Dinev",
                        Phone = "066666666",
                        Street = "trera ulica",
                        CityId = 1,
                        Salary = 1000
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.Employees.AddOrUpdate(
                        c => c.Phone,
                        new Employee
                        {
                            Id = i + 100,
                            FirstName = "FirstName_" + index,
                            LastName = "LastName_" + index,
                            Phone = "088888888_" + index,
                            Street = "Street_" + index,
                            Salary = i * 1000,
                            CityId = i
                        }
                    );
                }
            }
        }

        private void AddVehicles()
        {
            if (_isRealData)
            {
                _context.Vehicles.AddOrUpdate(
                    x => x.VehicleModel,
                    new Vehicle
                    {
                        Id = 1,
                        VehicleModel = "M6",
                        EngineName = "M6 Cool EngineName",
                        EngineType = "M6 Cool EngineType",
                        Transmission = "Manual Transmission",
                        VehicleManufacturerId = 1,
                        VehicleTypeId = 1,
                        Modifination = "2.3 i",
                        Year = 2001,
                        Body = "M6 Body"
                    },
                    new Vehicle
                    {
                        Id = 2,
                        VehicleModel = "M5",
                        EngineName = "M5 Cool EngineName",
                        EngineType = "M5 Cool EngineType",
                        Transmission = "Manual Transmission",
                        VehicleManufacturerId = 1,
                        VehicleTypeId = 2,
                        Year = 2001,
                        Modifination = "1.6",
                        Body = "M5 Body"
                    },
                    new Vehicle
                    {
                        Id = 3,
                        VehicleModel = "GOLF",
                        EngineName = "Golf Cool EngineName",
                        EngineType = "Golf Cool EngineType",
                        Transmission = "Manual Transmission",
                        VehicleManufacturerId = 2,
                        VehicleTypeId = 1,
                        Year = 1989,
                        Modifination = "3.5 TDI",
                        Body = "GOLF Body"
                    },

                    new Vehicle
                    {
                        Id = 4,
                        VehicleModel = "Samara",
                        EngineName = "Lada EngineName",
                        EngineType = "Lada EngineType",
                        Transmission = "Manual Transmission",
                        VehicleManufacturerId = 4,
                        VehicleTypeId = 1,
                        Year = 1977,
                        Modifination = "1.8",
                        Body = "Samara Body"
                    },

                    new Vehicle
                    {
                        Id = 5,
                        VehicleModel = "Bravo",
                        EngineName = "Reno EngineName",
                        EngineType = "Reno EngineType",
                        Transmission = "Manual Transmission",
                        VehicleManufacturerId = 11,
                        VehicleTypeId = 1,
                        Year = 1995,
                        Modifination = "2.5 TDI",
                        Body = "Bravo Body"
                    },

                    new Vehicle
                    {
                        Id = 6,
                        VehicleModel = "Ford Focus",
                        EngineName = "Ford EngineName",
                        EngineType = "Ford EngineType",
                        Transmission = "Auto Transmission",
                        VehicleManufacturerId = 6,
                        VehicleTypeId = 1,
                        Year = 1997,
                        Modifination = "1.9 TDI",
                        Body = "Ford Body"
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.Vehicles.AddOrUpdate(
                        x => x.VehicleModel,
                        new Vehicle
                        {
                            Id = i,
                            VehicleModel = "VehicleModel_" + index,
                            EngineName = "EngineName_" + index,
                            EngineType = "EngineType_" + index,
                            Transmission = "Transmission_" + index,
                            VehicleManufacturerId = 1,
                            VehicleTypeId = 1,
                            Year = 1990 + i,
                            Modifination = "Modifination_" + index,
                            Body = "Body_" + index,
                            Fuel = "Fuel_" + index,
                        }
                    );
                }
            }
        }

        private void AddReplacementParts()
        {
            if (_isRealData)
            {
                _context.ReplacementParts.AddOrUpdate(
                    x => x.PartNumber,
                    new ReplacementPart
                    {
                        Id = 1,
                        PartNumber = "PartNumber123abvc",
                        Name = "Break disk",
                        Price = 444,
                        ReplacementPartCategoryId = 1,
                        ReplacementPartManufacturerId = 3,

                    },
                    new ReplacementPart
                    {
                        Id = 2,
                        PartNumber = "PartNumbera123abvc",
                        Name = "1Break disk",
                        Price = 22,
                        ReplacementPartCategoryId = 4,
                        ReplacementPartManufacturerId = 4,
                    },
                    new ReplacementPart
                    {
                        Id = 3,
                        PartNumber = "PartNumber3a123abvc",
                        Name = "3Break disk",
                        Price = 11,
                        ReplacementPartCategoryId = 5,
                        ReplacementPartManufacturerId = 6,
                    },
                    new ReplacementPart
                    {
                        Id = 4,
                        PartNumber = "PartNumberrea123abvc",
                        Name = "21Break disk2",
                        Price = 333,
                        ReplacementPartCategoryId = 1,
                        ReplacementPartManufacturerId = 3,
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.ReplacementParts.AddOrUpdate(
                        x => x.PartNumber,
                        new ReplacementPart
                        {
                            Id = i,
                            PartNumber = "PartNumber_" + index,
                            Name = "Name_" + index,
                            Price = 2 * i,
                            ReplacementPartCategoryId = 1,
                            ReplacementPartManufacturerId = 3,
                        }
                    );
                }
            }
        }

        private void AddCustomerVehicles()
        {
            if (_isRealData)
            {
                _context.CustomerVehicles.AddOrUpdate(
                    x => x.RegistrationNumber,
                    new CustomerVehicle
                    {
                        Id = 1,
                        PersonId = 1,
                        ColorId = 1,
                        RegistrationNumber = "PB1234MN",
                        VIN = "1B4HS48N32F178924",
                        VehicleId = 1
                    },
                    new CustomerVehicle
                    {
                        Id = 2,
                        RegistrationNumber = "PB5678MF",
                        VIN = "JW6DNK1E7SM035381",
                        PersonId = 2,
                        ColorId = 1,
                        VehicleId = 1
                    },
                    new CustomerVehicle
                    {
                        Id = 3,
                        RegistrationNumber = "PB1122MF",
                        VIN = "1GTBS19E2J8533591",
                        PersonId = 2,
                        ColorId = 1,
                        VehicleId = 1,
                    },
                    new CustomerVehicle
                    {
                        Id = 4,
                        ColorId = 5,
                        RegistrationNumber = "PB3344MM",
                        VIN = "JHMAD5422FC029295",
                        PersonId = 3,
                        VehicleId = 2
                    },
                    new CustomerVehicle
                    {
                        Id = 5,
                        ColorId = 2,
                        RegistrationNumber = "PB5566NN",
                        VIN = "1FT8W3AT1FEC23381",
                        PersonId = 3,
                        VehicleId = 3
                    },
                    new CustomerVehicle
                    {
                        Id = 6,
                        ColorId = 2,
                        RegistrationNumber = "CA7788NN",
                        VIN = "1FBNE31M63H072340",
                        PersonId = 3,
                        VehicleId = 4
                    },
                    new CustomerVehicle
                    {
                        Id = 7,
                        ColorId = 3,
                        RegistrationNumber = "PB9900PO",
                        VIN = "1FD8X3GT2CEB56754",
                        PersonId = 4,
                        VehicleId = 3
                    },
                    new CustomerVehicle
                    {
                        Id = 8,
                        ColorId = 2,
                        RegistrationNumber = "PB1010VF",
                        VIN = "2G1WW12E849393659",
                        PersonId = 4,
                        VehicleId = 3
                    },
                    new CustomerVehicle
                    {
                        Id = 9,
                        ColorId = 1,
                        RegistrationNumber = "PB2020MN",
                        VIN = "3GNGK26G11G261294",
                        PersonId = 4,
                        VehicleId = 3
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.CustomerVehicles.AddOrUpdate(
                        x => x.RegistrationNumber,
                        new CustomerVehicle
                        {
                            Id = i,
                            RegistrationNumber = "RegistrationNumber_" + index,
                            PersonId = 1,
                            VehicleId = 1,
                        }
                    );
                }
            }
        }

        private void AddRepairs()
        {
            if (_isRealData)
            {
                _context.Repairs.AddOrUpdate(
                    x => x.Name,
                    new Repair
                    {
                        Id = 1,
                        Name = "RepairName_1",
                        CustomerVehicleId = 1,
                        RepairTypeId = 1,
                        PersonId = 6,
                        Description = "test desc_1",
                        WorkingHours = 1,
                        Mileage = 111100,
                        IsCompleted = true
                    },
                    new Repair
                    {
                        Id = 2,
                        Name = "RepairName_2",
                        CustomerVehicleId = 2,
                        RepairTypeId = 2,
                        PersonId = 6,
                        Description = "test desc_2",
                        Mileage = 2211000
                    },
                    new Repair
                    {
                        Id = 3,
                        Name = "RepairName_3",
                        CustomerVehicleId = 2,
                        RepairTypeId = 3,
                        PersonId = 6,
                        Description = "test desc_3",
                        Mileage = 2233112
                    },
                    new Repair
                    {
                        Id = 4,
                        Name = "RepairName_4",
                        CustomerVehicleId = 1,
                        RepairTypeId = 1,
                        PersonId = 6,
                        Description = "test desc_4",
                        Mileage = 44332211
                    },
                    new Repair
                    {
                        Id = 5,
                        Name = "RepairName_5",
                        CustomerVehicleId = 1,
                        RepairTypeId = 1,
                        PersonId = 6,
                        Description = "test desc_1",
                        Mileage = 66554433
                    },
                    new Repair
                    {
                        Id = 6,
                        Name = "RepairName_6",
                        CustomerVehicleId = 1,
                        RepairTypeId = 1,
                        PersonId = 6,
                        Description = "test desc_1",
                        Mileage = 99887733
                    },
                    new Repair
                    {
                        Id = 7,
                        Name = "RepairName_7",
                        CustomerVehicleId = 1,
                        RepairTypeId = 2,
                        PersonId = 6,
                        Description = "test desc_1",
                        Mileage = 3322421
                    },
                    new Repair
                    {
                        Id = 8,
                        Name = "RepairName_8",
                        CustomerVehicleId = 3,
                        RepairTypeId = 3,
                        PersonId = 6,
                        Description = "test desc_22",
                        Mileage = 3544423
                    }
                );
            }
            else
            {
                for (int i = 1; i <= 100; i++)
                {
                    string index = i.ToString();
                    _context.Repairs.AddOrUpdate(
                        x => x.Name,
                        new Repair
                        {
                            Id = i,
                            Name = "RepairName_" + index,
                            CustomerVehicleId = 1,
                            RepairTypeId = 1,
                            PersonId = i + 100,
                            Description = "test desc_" + index,
                            Mileage = i + 1000
                        }
                    );
                }
            }
        }
    }
}
