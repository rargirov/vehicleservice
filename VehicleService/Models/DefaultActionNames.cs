﻿namespace VehicleService.Models
{
    public static class DefaultActionNames
    {
        public const string Index = "Index";
        public const string Create = "Create";
        public const string Edit = "Edit";
        public const string Delete = "Delete";
        public const string Details = "Details";
        public const string Autocomplete = "Autocomplete";
        public const string ListAjax = "ListAjax";
    }
}