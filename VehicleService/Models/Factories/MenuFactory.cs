﻿using Blueprints.Interfaces;
using Blueprints.Models;
using System.Collections.Generic;

namespace VehicleService.Models.Factories
{
    public static class MenuFactory
    {
        public static IMenuItem BuildMenuItem(string text, string action, string controller = null, object routeValues = null)
        {
            return new MenuItem()
            {
                Text = text,
                ActionName = action,
                ControllerName = controller,
                RouteValues = routeValues
            };
        }

        public static IList<IMenuItem> BuildMenu(params IMenuItem[] menuItems)
        {
            var menu = new List<IMenuItem>();
            menu.AddRange(menuItems);
            return menu;
        }

        public static IEnumerable<IMenuItem> BuildMenuIndex(object routeValues = null)
        {
            return MenuFactory.BuildMenu(MenuFactory.MenuItemIndex(routeValues));
        }

        public static IEnumerable<IMenuItem> BuildMenuCreate(object routeValues = null)
        {
            return MenuFactory.BuildMenu(MenuFactory.MenuItemCreate(routeValues));
        }

        public static IEnumerable<IMenuItem> BuildMenuDetails(object routeValues)
        {
            var menuItems = new[]
            {
                MenuFactory.MenuItemCreate(),
                MenuFactory.MenuItemEdit(routeValues),
                MenuFactory.MenuItemDelete(routeValues),
                MenuFactory.MenuItemIndex(),
            };
            return MenuFactory.BuildMenu(menuItems);
        }

        public static IEnumerable<IMenuItem> BuildMenuEdit(object routeValues)
        {
            var menuItems = new[]
            {
                MenuFactory.MenuItemCreate(),
                MenuFactory.MenuItemDelete(routeValues),
                MenuFactory.MenuItemIndex(),
            };
            return MenuFactory.BuildMenu(menuItems);
        }

        public static IEnumerable<IMenuItem> BuildMenuDelete(object routeValues)
        {
            var menu = MenuFactory.BuildMenu();
            menu.Add(MenuFactory.MenuItemCreate());
            menu.Add(MenuFactory.MenuItemEdit(routeValues));
            menu.Add(MenuFactory.MenuItemIndex());

            return menu;
        }


        public static IMenuItem MenuItemCreate(object routeValues = null)
        {
            return MenuFactory.BuildMenuItem("Create", "Create", null, routeValues);
        }

        public static IMenuItem MenuItemIndex(object routeValues = null)
        {
            return MenuFactory.BuildMenuItem("Back to list", "Index", null, routeValues);
        }

        public static IMenuItem MenuItemEdit(object routeValues)
        {
            return MenuFactory.BuildMenuItem("Edit", "Edit", null, routeValues);
        }

        public static IMenuItem MenuItemDelete(object routeValues)
        {
            return MenuFactory.BuildMenuItem("Delete", "Delete", null, routeValues);
        }
    }
}