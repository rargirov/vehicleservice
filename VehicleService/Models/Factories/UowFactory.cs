﻿using DbRepository;
using DbRepository.Interfaces;
using VehicleService.Models.DB;

namespace VehicleService.Models.Factories
{
    public static class UowFactory
    {
        public static IUnitOfWork BuildUnitOfWork(IVehicleServiceDb db = null)
        {
            if (db == null)
            {
                db = new VehicleServiceDb();
            }

            return new UnitOfWork(db);
        }
    }
}
