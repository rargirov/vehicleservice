﻿using Blueprints.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace VehicleService.Models
{
    public class PriceCalculator
    {
        public static decimal CalculateOrderTotalPrice(decimal additionalPrice, Repair repair)
        {
            decimal totalPrice = additionalPrice + repair.LaborPrice + repair.ReplacementPartsSum;
            return totalPrice;
        }

        public static decimal CalculateLaborPrice(decimal workingHours, decimal pricePerHour)
        {
            decimal laborPrice = workingHours * pricePerHour;
            return laborPrice;
        }

        public static decimal CalculateReplacementPartsPrice(Dictionary<ReplacementPart, int> replacementParts)
        {
            decimal replacementPartsPrice = replacementParts.Sum(x => (x.Key.Price * x.Value));
            return replacementPartsPrice;
        }
    }
}