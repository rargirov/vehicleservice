﻿$(function () {

    var ajaxFormSubmit = function () {
        var $form = $(this);

        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr("data-vs-target"));
            var $newHtml = $(data);
            $target.replaceWith($newHtml);
            $newHtml.effect("highlight");
        });

        return false;
    };

    var submitAutocompleteForm = function (event, ui) {

        var $input = $(this);
        $input.val(ui.item.label);

        var $form = $input.parents("form:first");
        $form.submit();
    };

    var createAutocomplete = function () {
        var $input = $(this);

        var options = {
            source: $input.attr("data-vs-autocomplete"),
            select: submitAutocompleteForm
        };

        $input.autocomplete(options);
    };

    var getPage = function () {
        var $a = $(this);
        var url = $a.attr("href");
        if (url) {
            var options = {
                url: url,
                data: $("form").serialize(),
                type: "get"
            };

            $.ajax(options).done(function (data) {
                var target = $a.parents("div.pagedList").attr("data-vs-target");
                $(target).replaceWith(data);
            });
        }
        
        return false;

    };

    var updateCitiesDropdown = function () {
        var countryId = $(this).find(":selected").val();
        if (countryId) {

            var options = {
                url: "/City/ListByCountry",
                data: { countryId: countryId },
                type: "get"
            };
            $.ajax(options).done(function (data) {
                var target = $("#CityId");
                target.html('');
                var options = '';
                for (var i = 0; i < data.length; i++) {
                    options += '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                }
                target.append(options);
            });

            $(".hidden").show();
        } else {
            $(".hidden").hide();
        }

        return false;
    }

    var updateVehiclesDropdown = function () {
        var manufacturerId = $(this).find(":selected").val();
        if (manufacturerId) {
            var options = {
                url: "/Vehicle/ListByManufacturer",
                data: { manufacturerId: manufacturerId },
                type: "get"
            };

            $.ajax(options).done(function (data) {
                var target = $("#VehicleId");
                target.html('');
                var options = '';
                for (var i = 0; i < data.length; i++) {
                    options += '<option value="' + data[i].Value + '">' + data[i].Text + '</option>';
                }
                target.append(options);
            });
            $(".hidden").show();

        } else {
            $(".hidden").hide();
        }

        return false;
    }

    $("form[data-vs-ajax='true']").submit(ajaxFormSubmit);
    $("input[data-vs-autocomplete]").each(createAutocomplete);

    $(".main-content").on("click", ".pagedList a", getPage);
    $(".main-content").on("change", "#CountryId", updateCitiesDropdown);
    $(".main-content").on("change", "#VehicleManufacturerId", updateVehiclesDropdown);
});